#ifndef _BOX_H_
#define _BOX_H_

#include <stdint.h>
#include <unistd.h>

#include <sys/syscall.h>
#include <sys/types.h>

#include <linux/landlock.h>

#define MOUNT_NS    (1 << 0)
#define UTS_NS      (1 << 1)
#define IPC_NS      (1 << 2)
#define USER_NS     (1 << 3)
#define PID_NS      (1 << 4)
#define NET_NS      (1 << 5)

#define ATTR_FORK   (1 << 0)

struct {
    uint8_t ns;
    uint8_t attr;

    struct ruleset {
        uint16_t mode;
        int fd;
    } ruleset;

    struct rule {
        uint16_t mode;
        char *pathname;
    } *rules;
    int rllen;

    char *chroot;
} state = {0};

/* TODO: Remove line length limit? */
#define LINE_LEN 2048

/* From samples/landlock/sandboxer.c, Linux */
#ifndef landlock_create_ruleset
static inline int landlock_create_ruleset(
                const struct landlock_ruleset_attr *const attr,
                        const size_t size, const __u32 flags
        )
{
    return syscall(__NR_landlock_create_ruleset, attr, size, flags);
        
}
#endif

#ifndef landlock_add_rule
static inline int landlock_add_rule(const int ruleset_fd,
                const enum landlock_rule_type rule_type,
                        const void *const rule_attr, const __u32 flags)
{
    return syscall(__NR_landlock_add_rule, ruleset_fd, rule_type,
            rule_attr, flags);
}
#endif

#ifndef landlock_restrict_self
static inline int landlock_restrict_self(const int ruleset_fd,
                const __u32 flags)
{
    return syscall(__NR_landlock_restrict_self, ruleset_fd, flags);    
}
#endif

#endif /* _BOX_H_ */
