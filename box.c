#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <sched.h>
#include <err.h>

#include <sys/stat.h>
#include <sys/prctl.h>
#include <sys/wait.h>

#include "box.h"

unsigned long long landlock_char_to_mode(char c)
{
    switch (c) {
    case 'r':
        return LANDLOCK_ACCESS_FS_READ_FILE;
    case 'w':
        return LANDLOCK_ACCESS_FS_WRITE_FILE;
    case 'x':
        return LANDLOCK_ACCESS_FS_EXECUTE;
    case 'R':
        return LANDLOCK_ACCESS_FS_READ_DIR;
    case 'u':
        return LANDLOCK_ACCESS_FS_REMOVE_FILE;
    case 'U':
        return LANDLOCK_ACCESS_FS_REMOVE_DIR;
    case 'c':
        return LANDLOCK_ACCESS_FS_MAKE_CHAR;
    case 'd':
        return LANDLOCK_ACCESS_FS_MAKE_DIR;
    case 'f':
        return LANDLOCK_ACCESS_FS_MAKE_REG;
    case 's':
        return LANDLOCK_ACCESS_FS_MAKE_SOCK;
    case 'F':
        return LANDLOCK_ACCESS_FS_MAKE_FIFO;
    case 'b':
        return LANDLOCK_ACCESS_FS_MAKE_BLOCK;
    case 'l':
        return LANDLOCK_ACCESS_FS_MAKE_SYM;
    case '-':
        return 0;
    case '*':
        return LANDLOCK_ACCESS_FS_READ_FILE | LANDLOCK_ACCESS_FS_WRITE_FILE | LANDLOCK_ACCESS_FS_EXECUTE |
               LANDLOCK_ACCESS_FS_READ_DIR | LANDLOCK_ACCESS_FS_REMOVE_FILE | LANDLOCK_ACCESS_FS_REMOVE_DIR |
               LANDLOCK_ACCESS_FS_MAKE_CHAR | LANDLOCK_ACCESS_FS_MAKE_DIR | LANDLOCK_ACCESS_FS_MAKE_REG |
               LANDLOCK_ACCESS_FS_MAKE_SOCK | LANDLOCK_ACCESS_FS_MAKE_FIFO | LANDLOCK_ACCESS_FS_MAKE_BLOCK |
               LANDLOCK_ACCESS_FS_MAKE_SYM;
    default:
        errx(2, "unknown mode bit: `%c'", c);
        break;
    }
}

int main(int argc, char **argv)
{
    if (argc < 3) {
        fprintf(stderr, "usage: %s conf prog [args]\n", argv[0]);
        return 1;
    }

    FILE *conf = fopen(argv[1], "r");

    if (!conf)
        err(1, "failed to open config file");

    char buf[LINE_LEN];

    while (fgets(buf, LINE_LEN, conf)) {
        if (buf[0] == '#')
            continue;

        int len = strlen(buf);

        /* remove trailing newline */
        if (buf[len - 1] == '\n') {
            buf[len - 1] = 0;
            len--;
        }

        char *token = strtok(buf, " \t");

        /* empty line */
        if (!token)
            continue;

        if (strcmp(token, "mountns") == 0)
            state.ns |= MOUNT_NS;
        else if (strcmp(token, "utsns") == 0)
            state.ns |= UTS_NS;
        else if (strcmp(token, "ipcns") == 0)
            state.ns |= IPC_NS;
        else if (strcmp(token, "pidns") == 0)
            state.ns |= PID_NS;
        else if (strcmp(token, "netns") == 0)
            state.ns |= NET_NS;

        else if (strcmp(token, "chroot") == 0) {
            char *dir = strtok(NULL, "");

            if (!dir)
                errx(1, "chroot directory not specified");

            if (state.chroot)
                free(state.chroot);

            int dirlen = strlen(dir);

            state.chroot = malloc(dirlen + 1);
            strncpy(state.chroot, dir, dirlen + 1);
        }

        else if (strcmp(token, "ruleset") == 0) {
            char *mode = strtok(NULL, " \t");

            for (int i = 0, c = mode[i]; c != 0; i++, c = mode[i])
                state.ruleset.mode |= landlock_char_to_mode(c);
        }

        else if (strcmp(token, "rule") == 0) {
            state.rules = realloc(state.rules, sizeof(struct rule) * (state.rllen + 1));
            memset(state.rules + state.rllen, 0, sizeof(struct rule));

            char *mode = strtok(NULL, " \t");

            for (int i = 0, c = mode[i]; c != 0; i++, c = mode[i])
                state.rules[state.rllen].mode |= landlock_char_to_mode(c);

            char *pathname = strtok(NULL, "");

            int pathnamelen = strlen(pathname);

            state.rules[state.rllen].pathname = malloc(pathnamelen + 1);
            strncpy(state.rules[state.rllen].pathname, pathname, pathnamelen + 1);

            state.rllen++;
        }

        else if (strcmp(token, "fork") == 0)
            state.attr |= ATTR_FORK;

        else
            warnx("unknown configuration file entry: `%s'", token);
    }

    if (!feof(conf))
        err(1, "failed to read config file");

    if (state.chroot) {
        if (chroot(state.chroot) == -1)
            err(2, "failed to chroot");

        free(state.chroot);
    }

    if (state.ns & MOUNT_NS && unshare(CLONE_NEWNS) == -1)
        err(2, "failed to unshare mount namespace");
    if (state.ns & UTS_NS && unshare(CLONE_NEWUTS) == -1)
        err(2, "failed to unshare uts namespace");
    if (state.ns & IPC_NS && unshare(CLONE_NEWIPC) == -1)
        err(2, "failed to unshare ipc namespace");
    if (state.ns & USER_NS && unshare(CLONE_NEWUSER) == -1)
        err(2, "failed to unshare user namespace");
    if (state.ns & PID_NS && unshare(CLONE_NEWPID) == -1)
        err(2, "failed to unshare pid namespace");
    if (state.ns & NET_NS && unshare(CLONE_NEWNET) == -1)
        err(2, "failed to unshare network namespace");

    if (state.rules) {
        struct landlock_ruleset_attr ruleset_attr = {
            .handled_access_fs = state.ruleset.mode,
        };

        state.ruleset.fd = landlock_create_ruleset(&ruleset_attr, sizeof(ruleset_attr), 0);

        if (state.ruleset.fd == -1)
            err(2, "failed to create ruleset");

        for (int i = 0; i < state.rllen; i++) {
            struct landlock_path_beneath_attr path_beneath = {
                .allowed_access = state.rules[i].mode,
            };

            struct stat st;

            if (stat(state.rules[i].pathname, &st) == -1)
                err(2, "failed to stat `%s'", state.rules[i].pathname);

            if ((st.st_mode & S_IFMT) == S_IFDIR)
                path_beneath.parent_fd = open(state.rules[i].pathname, O_PATH | O_CLOEXEC);
            else
                path_beneath.parent_fd = open(state.rules[i].pathname, O_CLOEXEC);

            if (path_beneath.parent_fd == -1)
                err(2, "failed to open `%s'", state.rules[i].pathname);

            int ret = landlock_add_rule(state.ruleset.fd, LANDLOCK_RULE_PATH_BENEATH, &path_beneath, 0);

            if (ret)
                err(2, "failed to add rule to ruleset");

            close(path_beneath.parent_fd);
        }

        if (prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0))
            err(2, "failed to restrict thread privileges");

        if (landlock_restrict_self(state.ruleset.fd, 0))
            err(2, "failed to enforce ruleset");

        close(state.ruleset.fd);

        for (int i = 0; i < state.rllen; i++)
            free(state.rules[i].pathname);

        free(state.rules);
    }

    /* mimicks unshare(1) behavior */
    if (state.attr & ATTR_FORK) {
        struct sigaction sa;

        sa.sa_handler = SIG_IGN;
        sigaction(SIGINT, &sa, NULL);
        sigaction(SIGTERM, &sa, NULL);

        int pid = fork();

        if (pid == 0)
            execvp(argv[2], argv + 2);
        else
            wait(NULL);
    } else {
        execvp(argv[2], argv + 2);
    }
}
