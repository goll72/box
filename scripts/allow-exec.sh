#!/bin/sh

# Creates a config file `$1` that will allow the execution of programs passed as arguments
# DEPS: GNU grep, file, ldd

exec 8<&-
exec 8>"$1"

echo 'ruleset *' >&8

shift

while [ $# -gt 0 ]; do
    FILE="$(file -b "$1")"

    grep 'statically linked' << EOF
$FILE
EOF

    if [ $? -ne 0 ]; then
        LDD="$(ldd "$1" 2>/dev/null | sed 's/^\t//g ; s/(0x.*)$//g ; s/.*=> //g' | grep -v 'linux-vdso.so')"

        for i in $LDD; do
            printf 'rule rx %s\n' "$i" >&8
        done
    fi

    printf 'rule rx %s' "$1" >&8

    shift
done
