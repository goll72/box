CFLAGS ?= -O2
CFLAGS += -D_GNU_SOURCE -D_FORTIFY_SOURCE=2
PREFIX = /usr/local

box:
	$(CC) $(CFLAGS) box.c -o box

clean:
	rm -f box

install:
	cp box $(DESTDIR)$(PREFIX)/bin
