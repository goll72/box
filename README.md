box
===

A simple containerization software that uses landlock and namespaces.

## Usage

box uses a configuration file, and is invoked as such:

```
box config-file program [args] ...
```

Each line in the configuration file represents an entry. Empty lines or lines starting with `#` are ignored. Spaces/tabs can be used as delimiters.


### Namespaces

The following keywords are used for namespaces:

```
mountns | utsns | ipcns | userns | pidns | netns
```

### Chroot

```
chroot pathname
```

### Landlock

For landlock, the following keywords are used:

```
ruleset mode
rule mode pathname
```

Where `mode` is one or more of the following filesystem access mode characters:

* `r` - read file
* `w` - write file
* `x` - execute
* `R` - read directory
* `u` - remove file
* `U` - remove directory
* `c` - create character device
* `d` - create directory
* `f` - create file
* `s` - create socket
* `F` - create fifo
* `b` - create block device
* `l` - create symlink
* `*` - all of the above
* `-` - none

When Landlock is enforced, all filesystem acces attempts not explicitly allowed in a rule will be denied, and it cannot be relaxed.

`ruleset` may be used only once to specify the filesystem access modes that will be handled and blocked if not explicitly allowed by a rule, but there is no limit on how many `rule`s there can be.

The syntax for the `ruleset` and `rule` keywords may change as landlock is expanded to handle other types of access control.

### Misc.

```
fork
```

Makes box fork before executing the program. Useful for PID namespaces. Has the same behavior as `unshare(1)`'s `-f` option.
